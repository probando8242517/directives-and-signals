import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styles: ``
})
export class ProductPageComponent {

  private fb = inject ( FormBuilder);

  public color:string = 'green';

  public myForm: FormGroup = this.fb.group({
    _name: ['', [Validators.required, Validators.minLength(6), Validators.email]],
    get name() {
      return this._name;
    },
    set name(value) {
      this._name = value;
    },
  });


  changeColor() {
    const color = '#xxxxxx'.replace(/x/g, y=>(Math.random()*16|0).toString(16));
    this.color = color;
  
  }



}
