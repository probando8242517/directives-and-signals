import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductsRoutingModule } from "./products-routing.module";
import { ProductPageComponent } from "./pages/product-page.component";
import { ReactiveFormsModule } from "@angular/forms";
import { JsonPipe } from '@angular/common';
import { SharedModule } from "../shared/shared.module";
@NgModule({
    declarations: [

      ProductPageComponent
    ],
    imports: [
        CommonModule,
        ProductsRoutingModule,
        ReactiveFormsModule,
        JsonPipe,
        SharedModule,
    ]
})
export class ProductsModule {}